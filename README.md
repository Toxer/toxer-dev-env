# Toxer Development Environment
Setup and maintain your local Toxer development environment for Toxer. Currently only Linux/Unix is supported while more systems may follow.

## Pre-Requisites

Tool|Min. Version (Desktop)|Min. Version (Sailfish)
----|----|----
GCC/Clang | C++17 | <--
[CMake](https://cmake.org/) | 3.23 | 3.11
[Qt](https://www.qt.io/) | 6.4 | 5.6.3 (due to Qt license)

Update to the latest (stable) versions available for a painless development experience!

## C++ Environment Setup Guide

Download and install [Qt](https://www.qt.io/).

### Ubuntu/Debian

```bash
# installs gcc, make, glibc, …
sudo apt install build-essential
```

I do not recommend to install the (outdated) pre-packaged CMake versions on those distributions.
Instead you can download the latest available binaries from the [CMake download site](https://cmake.org/download/).
Extract the "tar.gz" into a directory of choice and point the `$PATH` environment to that location.

Extract the downloaded binary to a location of choice:

```bash
cd ~                             # 1. cd to "home"
tar xf <cmake-archive>.tar.gz    # 2. extract the downloaded archive (creates a cmake… dir)
mv <cmake-archive> cmake         # 3. rename the drectory
```

Edit your ~/.bashrc (~/.zshrc respectively) and extend your PATH environment.

```bash
export PATH=$HOME/cmake/bin:$PATH
```

To verify it works open a (new) terminal and check the CMake version is up-to-date.

```bash
cmake --version
```

## Getting Started with Toxer Development
Start by cloning this repository into a location of choice. Afterwards setup the project sources:

```bash
cd /path/to/toxer-dev-env
make prepare
```

This will clone Toxer repositories from the "upstream" repository.
For a first local test run you can use the Flatpak build.

```bash
make flatpak-test-run
```

To open the project in QtCreator only a few more steps are required.

```bash
make prepare
make install-toxcore-dev-linux
```

Then open the file `ToxerDesktop/CMakeLists.txt` in QtCreator. Setup a build location and you are ready to go.


# Contributing to Toxer
You are welcome to contribute to the Toxer project. To do so please fork the repository (for example ToxerDesktop) and add an "origin" Git remote.

```bash
git -C ToxerDesktop remote add origin git@gitlab.com:<your-user-name>/toxer-desktop.git
```

**Enjoy Toxer development!**
