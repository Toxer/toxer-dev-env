TOX_VERSION = 0.2.18
TOX = tox-$(TOX_VERSION)

HEADERS = /usr/local/include
LIBS = /usr/local/lib
LIBS64 = /usr/local/lib64
TOX_HEADERS = $(HEADERS)/tox

TOX_BUILD = $(TOX)/build
LOGBUILD = toxcore_build.log
LOGINSTALL = toxcore_install.log

TOXER_CORE = ToxerCore
TOXER_DESK = ToxerDesktop
TOXER_SFOS = ToxerSFOS


.PHONY: clean
.SILENT: show-toxcore-cmake-options


$(TOX):
	git clone -q --branch=v$(TOX_VERSION) --depth=1 --shallow-submodules --recursive git@github.com:toktok/c-toxcore $(TOX)

$(TOXER_CORE):
	git clone -q --origin upstream git@gitlab.com:toxer/toxer-core.git $(TOXER_CORE)

$(TOXER_DESK):
	git clone -q --origin upstream git@gitlab.com:toxer/toxer-desktop.git $(TOXER_DESK)

$(TOXER_SFOS):
	git clone -q --origin upstream git@gitlab.com:toxer/toxer-sfos.git $(TOXER_SFOS)


clone: $(TOX) $(TOXER_CORE) $(TOXER_DESK) $(TOXER_SFOS)


uninstall-toxcore-dev-linux:
	@echo "Removing installed toxcore development libraries and headers…"
	sudo rm -rf $(TOX_HEADERS)
	sudo rm -f $(LIBS)/libtoxcore.* \
	  $(LIBS)/pkgconfig/toxcore.pc \
	  $(LIBS64)/libtoxcore.* \
	  $(LIBS64)/pkgconfig/toxcore.pc
	rm -f ${LOGINSTALL}


install-toxcore-dev-linux: $(TOX)
	@echo "Building toxcore in $(TOX_BUILD)…"
	mkdir -p $(TOX_BUILD)
	cmake -S $(TOX) -B $(TOX_BUILD) \
	  -DBOOTSTRAP_DAEMON=OFF \
	  -DDHT_BOOTSTRAP=OFF \
	  -DENABLE_STATIC=OFF \
	  $(TOX) > $(LOGBUILD)
	make -C $(TOX_BUILD) -j2 -s >> $(LOGBUILD)
	@echo
	@echo "Installing libtoxcore"
	sudo make -C $(TOX_BUILD) -s install > $(LOGINSTALL)
	@echo "See $(LOGINSTALL) for the file locations."
	@echo


show-toxcore-cmake-options: $(TOX)
	grep "option(" $(TOX)/CMakeLists.txt $(TOX)/cmake/*
	grep "set(.* CACHE" $(TOX)/CMakeLists.txt $(TOX)/cmake/*


clean:
	rm -f $(LOGBUILD)
	rm -rf tox-*/
	flatpak --user uninstall --delete-data --noninteractive org.toxer.Toxer 2>/dev/null || exit 0
	flatpak --user remote-delete toxer-dev 2>/dev/null || exit 0
	rm -rf build/flatpak*
	rm -rf .flatpak-builder/

prepare: clone
	ln -sf ../$(TOXER_CORE) $(TOXER_DESK)/$(TOXER_CORE) >/dev/null
	ln -sf ../$(TOXER_CORE) $(TOXER_SFOS)/$(TOXER_CORE) >/dev/null


flatpak-sdk: clone
	flatpak install --noninteractive org.kde.Sdk//6.4
	flatpak install --noninteractive org.kde.Platform//6.4

flatpak-sdk-uninstall:
	flatpak uninstall --delete-data --noninteractive org.kde.Sdk//6.4 2>/dev/null || exit 0
	flatpak uninstall --delete-data org.kde.Platform//6.4 2>/dev/null || exit 0

flatpak: flatpak-sdk
	flatpak-builder --force-clean build/flatpak packaging/org.toxer.Toxer.yml

flatpak-run: flatpak-sdk
	flatpak-builder --force-clean --repo=build/flatpak-repo build/flatpak packaging/org.toxer.Toxer.yml
	flatpak --user remote-add --if-not-exists --no-gpg-verify toxer-dev build/flatpak-repo
	flatpak --user install --reinstall toxer-dev org.toxer.Toxer
	flatpak --user run org.toxer.Toxer

run: flatpak-run
