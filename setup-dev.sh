#!/bin/sh
set -e

if [ "$DEBUG" = "TRUE" ]; then
  set -x
fi

# local
W="${W:-$(dirname `readlink -f $0`)}"
TOXER=$W/toxer_core
TOXER_DESK=$W/ToxerDesktop
TOXER_DESK_BUILD=$W/ToxerDesktop/build
TOXER_SFOS=$W/ToxerSFOS

# Git resources
TOXER_URL=https://gitlab.com/Toxer
TOXER_CORE_URL=$TOXER_URL/toxer-core.git
TOXER_DESK_URL=$TOXER_URL/toxer-desktop.git
TOXER_SFOS_URL=$TOXER_URL/toxer-sfos.git

usage() {
  echo ""
  echo "Toxer Developer Maintenance Tool."
  echo "Usage: $0 <command> [options]"
  echo ""
  echo "Commands:"
  echo " * init [all=default, sailfish, desktop, android]"
  echo "   Initialize the project directory structure."
  echo " * relink"
  echo "   Creates/repairs toxer_core symbolic links in subdirectories."
  echo ""
  echo "Online Resources:"
  echo " 1. Code     -> $TOXER_URL"
  echo " 2. Website  -> <TBD>"
  echo ""
}

die(){
# Prints an error text in red color and exits with RC 1.
  echo -e "\033[1;31mError: $1\033[m" 1>&2 && usage && exit 1
}

toxer_info(){
# outputs info in color light cyan
  echo -e "\n\033[1;36m$1\033[m"
}
toxer_step(){
# outputs toxer step info in color light cyan
  echo -en "\033[1;36m$1\033[m"
}
toxer_done(){
# outputs info in color green
  local str="${1:Done}"
  echo -e "\033[0;32m$str\033[m"
}

toxer_symlinks(){
# create symlinks to toxer_core
  toxer_step "Creating symbolic links to $TOXER…"
  ln -sf ../toxer_core $TOXER_DESK/ToxerCore >/dev/null
  ln -sf ../toxer_core $TOXER_SFOS/ToxerCore >/dev/null
  toxer_done "  [OK]"
}

toxer_symlinks(){
# create symlinks to toxer_core
  toxer_step "Creating symbolic links to $TOXER…"
  if [ -d "$TOXER_DESK" ]; then ln -sf ../toxer_core $TOXER_DESK/ToxerCore >/dev/null; fi
  if [ -d "$TOXER_SFOS" ]; then ln -sf ../toxer_core $TOXER_SFOS/ToxerCore >/dev/null; fi
  toxer_done " \t[OK]"
}

toxer_builddirs(){
# create build directories for out-of-source builds
  if [ -d "$TOXER_DESK" ]; then
    toxer_step "Creating CMake out-of-source build directory in $TOXER_DESK…"
    mkdir -p $TOXER_DESK_BUILD
    toxer_done "  [OK]"
  fi
  # Note: TOXER_SFOS needs to be build from SailfishSDK
}

toxer_git_up(){
  toxer_step "Cloning upstream Git repository $1\033[m\nto ${2}…"
  git clone -q --origin upstream $1 $2 1>/dev/null
  toxer_done " \t[OK]\n"
}

toxer_init(){
  toxer_info "Preparing Toxer project in $W"
  echo ""

  # update upstream Git reporitories
  toxer_git_up $TOXER_CORE_URL $TOXER
  toxer_git_up $TOXER_DESK_URL $TOXER_DESK
  toxer_git_up $TOXER_SFOS_URL $TOXER_SFOS

  # create additional helpers in build directories
  toxer_symlinks
  toxer_builddirs
}

toxer_purge(){
  echo "TODO"
  # TODO: wait for user input…
  #echo "This will delete all local Toxer sources. Sure? [y/N]: "
  # git clean -dxf
  # Workaround: remove dirs manually
  #rm -rf $TOXER $TOXER_DESK $TOXER_SFOS
}

if [ $# -lt 1 ] && [ -z "$DEBUG" ]; then die "Toxer maintenance tool requires a command."; fi
case "$1" in
init)   toxer_init ${@:2} ;;
relink) toxer_symlinks ;;
purge)  toxer_purge ;;
*)      if [ -z "$DEBUG" ]; then die "Unknown command \"$1\"."; else echo "Debugging…"; exit 0; fi ;;
esac

toxer_done "\nFinished! Happy Toxer'ing!\n"
