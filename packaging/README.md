# How to Create Packages for the Toxer Application

A Linux Flatpak package can be build and locally installed for testing.

```bash
cd ..  # if needed
make flatpak-test-run
```


# Linux

Currently a Flatpak package has to be build locally.


## Flatpak

Make sure Flatpak is installed on your system.

```bash
make flatpak-sdk
make flatpak
make flatpak-test-run
```

Uploding to Flathub is work in progress and therefore no official Toxer packages exist yet.


## Snap

Currently not available. :(
You are welcome to help out on this end!


## deb

Not available yet. :( Please help out on that end!


## rpm

Not available yet. :( Please help out on that end!


## PKGBUILD

Not available yet. :( Please help out on that end!


# Mac
No DMG available yet. :( Please help out on that end!


# Windows
Not available yet. :( Please help out on this end!
